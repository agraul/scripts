#!/bin/bash

EXCLUDE_FILE="$HOME/exclude-backup.txt"

rsync --archive --verbose --exclude-from="$EXCLUDE_FILE" / "$1"
