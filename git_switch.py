#!/usr/bin/python3
"""
git_switch.py

Switch work and private git email address easily.
"""

import argparse
import sys
import os

EMAIL_MAP = {'work': 'agraul@suse.com',
             'private': 'mail@agraul.de'}

PARSER = argparse.ArgumentParser()
PARSER.add_argument('ACCOUNT', help='What ACCOUNT to use (work/private)')
ARGS = PARSER.parse_args()

ACCOUNT = ARGS.account

if ACCOUNT not in EMAIL_MAP:
    sys.exit(1)

os.system(f"git config --global user.email {EMAIL_MAP[ACCOUNT]}")
